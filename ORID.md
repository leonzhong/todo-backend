### Objective:

- Today we practiced front-end and back-end project integration, connecting the TODO project to the back-end code we wrote ourselves.

- We reviewed React-related knowledge using concept maps.

- We introduced agile development concepts, learning about elevator pitches, MVPs, user stories, and user journeys.

### Reflective:

- The three weeks went by quickly. I learned a lot of knowledge and felt very fulfilled.
### Interpretive:

- Today I was mainly responsible for explaining React-related concept maps and elevator pitches/MVP concepts. I feel I have made some progress, but I still have deficiencies.

### Decisional:

- Next week we will develop a team project. I hope I can continue to improve.