package com.afs.restapi.service;

import com.afs.restapi.entity.Todo;
import com.afs.restapi.exception.TodoNotFoundException;
import com.afs.restapi.repository.JPATodoRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class TodoService {
    private final JPATodoRepository jpaTodoRepository;

    public TodoService(JPATodoRepository jpaTodoRepository) {
        this.jpaTodoRepository = jpaTodoRepository;
    }

    public List<Todo> getTodos() {
        return jpaTodoRepository.findAll();
    }

    public Todo createTodo(Todo todo) {
        return jpaTodoRepository.save(todo);
    }

    public void removeTodo(Long id) {
        jpaTodoRepository.deleteById(id);
    }

    public Todo updateTodo(Long id, Todo todo) {
        Todo originTodo = findById(id);
        if (Objects.nonNull(todo.getDone())) {
            originTodo.setDone(todo.getDone());
        }
        if (Objects.nonNull(todo.getText())) {
            originTodo.setText(todo.getText());
        }
        return jpaTodoRepository.save(originTodo);
    }

    private Todo findById(Long id) {
        return jpaTodoRepository.findById(id).orElseThrow(TodoNotFoundException::new);
    }

    public List<Todo> getAllDoneTodos() {
        return jpaTodoRepository.findAllByDone(true);
    }

    public Todo getTodoById(Long id) {
        return findById(id);
    }
}
