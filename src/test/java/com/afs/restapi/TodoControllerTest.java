package com.afs.restapi;

import com.afs.restapi.entity.Todo;
import com.afs.restapi.repository.JPATodoRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@AutoConfigureMockMvc
public class TodoControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private JPATodoRepository jpaTodoRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        jpaTodoRepository.deleteAll();
    }

    @Test
    void should_create_todo() throws Exception {
        Todo todo = new Todo(null, "test", false, null);
        String writeValueAsString = objectMapper.writeValueAsString(todo);
        mockMvc.perform(post("/todos")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(writeValueAsString))
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value(todo.getText()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(todo.getDone()));
    }

    @Test
    void should_get_all_todos() throws Exception {
        Todo todo1 = new Todo(null, "1", false, null);
        Todo todo2 = new Todo(null, "2", true, null);
        Todo todo3 = new Todo(null, "3", true, null);

        jpaTodoRepository.save(todo1);
        jpaTodoRepository.save(todo2);
        jpaTodoRepository.save(todo3);


        mockMvc.perform(get("/todos"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(3))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(todo1.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].text").value(todo1.getText()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].done").value(todo1.getDone()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].id").value(todo2.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].done").value(todo2.getDone()))
        ;
    }

    @Test
    void should_get_todo_by_id() throws Exception {
        Todo todo1 = new Todo(null, "1", false, null);
        jpaTodoRepository.save(todo1);

        mockMvc.perform(get("/todos/" + todo1.getId()))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(todo1.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value(todo1.getText()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(todo1.getDone()))
        ;
    }

    @Test
    void should_delete_todo() throws Exception {
        Todo todo1 = new Todo(null, "1", false, null);
        jpaTodoRepository.save(todo1);

        mockMvc.perform(delete("/todos/{id}", todo1.getId()))
                .andExpect(MockMvcResultMatchers.status().is(204));
        assertTrue(jpaTodoRepository.findById(todo1.getId()).isEmpty());
    }

    @Test
    void should_update_todo() throws Exception {
        Todo todo1 = new Todo(null, "1", false, null);
        jpaTodoRepository.save(todo1);

        Todo todo2 = new Todo(todo1.getId(), "test2", true, null);
        String writeValueAsString = objectMapper.writeValueAsString(todo2);

        mockMvc.perform(put("/todos/{id}", todo1.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(writeValueAsString))
                .andExpect(MockMvcResultMatchers.status().is(204))
        ;
        Optional<Todo> optionalTodo = jpaTodoRepository.findById(todo1.getId());
        assertTrue(optionalTodo.isPresent());
        Todo updatedTodo = optionalTodo.get();
        Assertions.assertEquals(todo1.getId(), updatedTodo.getId());
        Assertions.assertEquals(todo2.getText(), updatedTodo.getText());
        Assertions.assertEquals(todo2.getDone(), updatedTodo.getDone());
    }

    @Test
    void should_get_todo_done() throws Exception {
        Todo todo1 = new Todo(null, "1", false, null);
        Todo todo2 = new Todo(null, "2", true, null);
        Todo todo3 = new Todo(null, "3", true, null);
        jpaTodoRepository.save(todo1);
        jpaTodoRepository.save(todo2);
        jpaTodoRepository.save(todo3);

        mockMvc.perform(get("/todos/done"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(2))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(todo2.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].text").value(todo2.getText()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].done").value(todo2.getDone()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].id").value(todo3.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].text").value(todo3.getText()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].done").value(todo3.getDone()));
    }

}
